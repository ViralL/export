'use strict';

// ready
$(document).ready(function() {

    var s = skrollr.init();

    // anchor
    $(".anchor").on("click","a", function (event) {
        $('a').removeClass('active');
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $(this).addClass('active');
        $('body,html').animate({scrollTop: top - 70}, 1000);
    });
    // anchor

    //Parallax
    $('section[data-type="background"]').each(function(){
        var $bgobj = $(this); // создаем объект
        var $window = $(window); // создаем объект
        $(window).scroll(function() {
            var yPos = -($window.scrollTop() / $bgobj.data('speed')); // вычисляем коэффициент
            // Присваиваем значение background-position
            var coords = 'center '+ yPos + 'px';
            // Создаем эффект Parallax Scrolling
            $bgobj.css({ backgroundPosition: coords });
        });
    });
    //Parallax

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    $('.close--js').click(function () {
        $(this).parent().slideToggle();
    });
    $('.main-nav__item--js > a').click(function () {
        $(this).parent().find('.sub-nav').slideToggle();
        return false;
    });
    // adaptive menu
    $(".catalog__item--js").on("mouseover", function () {
      $('.backgrounds__item').removeClass('active');
      var dataId = $(this).data('back');
      // alert(dataId);
      $('.'+dataId).addClass('active');
    });

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    var $status = $('.paging-info');
    var $slickElement = $('.slider');
    var $slickElementXs = $('.slider__xs');
    var $slickElementXXs = $('.slider__xxs');
    var $slickElements = $('.carousel');
     $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
         var i = (currentSlide ? currentSlide : 0) + 1;
         $status.html('<span>' + i + '</span> /' + slick.slideCount);
     });
     $slickElement.slick({
        autoplay: true,
        arrows: true,
        dots: false,
        nextArrow: $('.arr-info-prev'),
        prevArrow: $('.arr-info-next')
     });
     $slickElements.slick({
        autoplay: false,
        arrows: true,
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: $('.arr-info-prev'),
        prevArrow: $('.arr-info-next'),
        responsive: [
          {
            breakpoint: 1299,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
     });
     $slickElementXs.slick({
        mobileFirst: true,
        arrows: false,
        dots: true,
        responsive: [
          {
              breakpoint: 768,
              settings: "unslick"
          }
        ]
     });
    $slickElementXXs.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.html('<span>' + i + '</span> /' + slick.slideCount);
    });
     $slickElementXXs.slick({
        mobileFirst: true,
        arrows: true,
        dots: false,
        nextArrow: $('.arr-info-prev'),
        prevArrow: $('.arr-info-next'),
        responsive: [
            {
                breakpoint: 768,
                settings: "unslick"
            }
        ]
     });
     $('.slider-for').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: false,
       asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
       slidesToShow: 4,
       slidesToScroll: 1,
       asNavFor: '.slider-for',
       focusOnSelect: true
    });
    // slider

    // select {select2}
    //$('select').select2({});
    // select


    // animation
    $('.animated').appear(function() {
        var elem = $(this);
        var animation = elem.data('animation');

        if ( !elem.hasClass('visible') ) {
            var animationDelay = elem.data('animation-delay');
            if ( animationDelay ) {
                setTimeout(function(){
                    elem.addClass( animation + " visible" );
                }, animationDelay);

            } else {
                elem.addClass( animation + " visible" );
            }
        }
    });
    // animation

    // popup {magnific-popup}
    $('.popup').magnificPopup({
  		type: 'inline'
  	});
  	$(document).on('click', '.popup-modal-dismiss', function (e) {
  		e.preventDefault();
  		$.magnificPopup.close();
  	});
    // popup

    // mobile sctipts
    var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (screen_width <= 767) {}
    // mobile sctipts

});
// ready

$(window).scroll(function(){
    if ($(window).scrollTop() >= 430) {
       $('header').addClass('fixed');
    }
    else {
       $('header').removeClass('fixed');
    }
});


//map
function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 13,
        mapTypeControl: false,
        zoomControl: true,
        scrollwheel: false,
        styles: [
          {
            "featureType": "administrative",
            "elementType": "labels.text",
            "stylers": [
              {
              "weight": "0.01"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
              {
              "color": "#fcb643"
              }
            ]
          },
          {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
              {
              "saturation": 7
              },
              {
              "lightness": 19
              },
              {
              "visibility": "on"
              },
              {
              "color": "#ff7c00"
              }
            ]
          },
          {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
              {
              "saturation": -100
              },
              {
              "lightness": 100
              },
              {
              "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
              {
              "color": "#f3f3f3"
              }
            ]
          },
          {
            "featureType": "landscape",
            "elementType": "labels.text.fill",
            "stylers": [
              {
              "color": "#ff9f00"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
              {
              "saturation": -100
              },
              {
              "lightness": "100"
              },
              {
              "visibility": "off"
              },
              {
              "weight": "1.00"
              },
              {
              "gamma": "0.92"
              },
              {
              "hue": "#ff0000"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
              "saturation": -93
              },
              {
              "lightness": 31
              },
              {
              "visibility": "simplified"
              },
              {
              "color": "#e4e4e4"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
              {
              "hue": "#ffac00"
              },
              {
              "saturation": -93
              },
              {
              "lightness": 31
              },
              {
              "visibility": "on"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [
              {
              "hue": "#008eff"
              },
              {
              "saturation": -93
              },
              {
              "lightness": -2
              },
              {
              "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
              {
              "hue": "#007fff"
              },
              {
              "saturation": -90
              },
              {
              "lightness": -8
              },
              {
              "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
              {
              "hue": "#ff9200"
              },
              {
              "saturation": "100"
              },
              {
              "lightness": "35"
              },
              {
              "visibility": "on"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
              {
              "hue": "#ffb200"
              },
              {
              "saturation": "-60"
              },
              {
              "lightness": "45"
              },
              {
              "visibility": "simplified"
              }
            ]
          }
        ]
    };
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(59.91916157, 30.3251195),
        map: map,
        icon: "../images/marker.svg"
    });
}
//map
